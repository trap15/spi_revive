#!/bin/sh
###############################################################################
## SPI REVIVE
###############################################################################
## Copyright (C)2018 Alex "trap15" Marshall <trap15@raidenii.net>
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to
## deal in the Software without restriction, including without limitation the
## rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
## sell copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
## FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
## IN THE SOFTWARE.
###############################################################################
## Builds all patches and creates release package
###############################################################################

RELEASE_NAME=spi-revive-20181211
RELEASE_DIR=distrib/$RELEASE_NAME/

# Same list as in Makefile
GAME_SUPPORT="rdftj rdft2j"

# Clear release directory
rm -rf $RELEASE_DIR
mkdir -p $RELEASE_DIR

# Build and copy each patch
for i in $GAME_SUPPORT; do
	make clean
	make GAME=$i
	cp build/$i.hex $RELEASE_DIR
done

# Build and copy makerom
make clean
make build/makerom
cp build/* $RELEASE_DIR

# Archive ZIP
pushd distrib && zip -r -9 $RELEASE_NAME.zip $RELEASE_NAME/ && popd
