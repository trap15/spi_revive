///////////////////////////////////////////////////////////////////////////////
// SPI REVIVE
///////////////////////////////////////////////////////////////////////////////
// Copyright (C)2018 Alex "trap15" Marshall <trap15@raidenii.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////
// Settings that change between games
///////////////////////////////////////////////////////////////////////////////

#ifndef CONFIG_H_
#define CONFIG_H_

// Game-specific addresses and settings
#if defined(GAMEID_rdft2j)
# define PRGBASE_INJECTION    0x2A0FB0
// This is debugging strings, never normally seen :)
# define PRGBASE_MENU         0x2B12B0

# define FLASHING_INIT_ADDR   0x201BCB

# define SNDCMD_FLASH_START   0x2A1BBD
# define SNDCMD_ECHO          0x2A1BED
# define SNDCMD_FLASH_END     0x2A1C1B
# define SNDCMD_REGIONCHECK   0x2A1E31
# define SNDCMD_FLASH_ERASE   0x2A1E9B

# define TEXTDRAW_SETPOS      0x29FAF8
# define TEXTDRAW_SETATTR     0x29FB50
# define TEXTDRAW_PUTS_BIG    0x29FFA1
# define TEXTDRAW_PUTS        0x29FF85
# define TEXTDRAW_PUTNUM      0x2A009E
# define VBLANK_DWORD         0x36418

# define INJECTION_PREFIX .byte 0x08
# define INJECTION_SUFFIX .byte 0x75, 0x08, 0x5F

# define COLOR_MAIN     4
# define COLOR_MENU     4
# define COLOR_GOOD     2
# define COLOR_BAD      1
# define COLOR_NEUTRAL  0
///////////////////////////////////////////////////////////////////////////////
#elif defined(GAMEID_rdftj)
# define PRGBASE_INJECTION    0x26C5C0
# define PRGBASE_MENU         0x3DC000

# define FLASHING_INIT_ADDR   0x2017A7

# define SNDCMD_FLASH_START   0x26D676
# define SNDCMD_ECHO          0x26D6A6
# define SNDCMD_FLASH_END     0x26D6D4
# define SNDCMD_REGIONCHECK   0x26D7EE
# define SNDCMD_FLASH_ERASE   0x26D858

# define TEXTDRAW_SETPOS      0x26B32C
# define TEXTDRAW_SETATTR     0x26B384
# define TEXTDRAW_PUTS_BIG    0x26B7B1
# define TEXTDRAW_PUTS        0x26B795
# define TEXTDRAW_PUTNUM      0x26B8AE
# define VBLANK_DWORD         0x3692C

# define INJECTION_PREFIX .byte 0x5D, 0x08
# define INJECTION_SUFFIX .byte 0x75, 0x08

# define COLOR_MAIN     4
# define COLOR_MENU     4
# define COLOR_GOOD     2
# define COLOR_BAD      1
# define COLOR_NEUTRAL  0
///////////////////////////////////////////////////////////////////////////////
#else
# error "Unsupported game"
// To add a new game, copy one of the above, then match the addresses.
// Chances are games with different regions are _very_ close if not the same
// addresses. Additionally, each group of addresses are likely from the same
// object file, so if you find the lowest address one, chances are the other
// addresses are a similar offset to the other games.
//
// Probably a bunch of stuff needs to change to support horizontal games. Buy
// me one if you want me to do it :)
//
// For reference of what each address contains:
// - PRGBASE_INJECTION:  Base address for the injected code. This should be
//                       around where the game normally calls
//                       SNDCMD_REGIONCHECK for checking the flash.
// - PRGBASE_MENU:       Base address for new code. This should be a 700h byte
//                       unused area of the ROM.
// - FLASHING_INIT_ADDR: Draws game logo in the center. 0 means don't use.
// - SNDCMD_FLASH_START: Sends command 0E4h. This begins flashing mode.
// - SNDCMD_ECHO:        Sends command 0E6h. This writes one byte.
// - SNDCMD_FLASH_END:   Sends command 0E7h. This ends flashing mode.
// - SNDCMD_REGIONCHECK: Sends command 0E0h. This checks flashed game data.
// - SNDCMD_FLASH_ERASE: Sends command 0F9h, then 0E1h. This erases the flash.
// - TEXTDRAW_SETPOS:    Sets text drawing cursor position.
// - TEXTDRAW_SETATTR:   Sets text drawing attributes (for color).
// - TEXTDRAW_PUTS_BIG:  Draws text in double-size font.
// - TEXTDRAW_PUTS:      Draws text in normal font.
// - TEXTDRAW_PUTNUM:    Draws numbers in normal font.
// - VBLANK_DWORD:       Frame counter in RAM, used to detect vblank.
// - INJECTION_PREFIX:   Due to using .hex, everything must be 16 byte aligned.
//                       This is the prefix bytes for the injected code.
// - INJECTION_SUFFIX:   Due to using .hex, everything must be 16 byte aligned.
//                       This is the suffix bytes for the injected code.
//
// The color values are just luck of the draw, but the indices should map like
// so:
// - COLOR_MAIN:         Yellow
// - COLOR_MENU:         Yellow
// - COLOR_GOOD:         Green
// - COLOR_BAD:          Red
// - COLOR_NEUTRAL:      Grey
#endif

#if (PRGBASE_INJECTION & 0xF)
# error "PRGBASE_INJECTION must be 16 byte aligned"
#endif
#if (PRGBASE_MENU & 0xF)
# error "PRGBASE_MENU must be 16 byte aligned"
#endif

#ifndef LINKER_SCRIPT
// Length of one line of text
#define LINE_LENGTH 30

// Indentation from horizontal edge
#define X_INDENT 2

// Setup region ID count, first, and last.
#define REGION_ATTR(count, front_cull, back_cull) .equ REGION_ID_COUNT, count
#define REGION(id, namelen, name)
#include "spi_region_table.inc.h"
#undef REGION
#undef REGION_ATTR
#define REGION_ATTR(count, front_cull, back_cull) .equ REGION_ID_FIRST, front_cull
#define REGION(id, namelen, name)
#include "spi_region_table.inc.h"
#undef REGION
#undef REGION_ATTR
#define REGION_ATTR(count, front_cull, back_cull) .equ REGION_ID_LAST, count-back_cull
#define REGION(id, namelen, name)
#include "spi_region_table.inc.h"
#undef REGION
#undef REGION_ATTR

// We will never use region_attr ever again, so remove it now
#define REGION_ATTR(count, front_cull, back_cull)
#endif

#endif
