///////////////////////////////////////////////////////////////////////////////
// SPI REVIVE
///////////////////////////////////////////////////////////////////////////////
// Copyright (C)2018 Alex "trap15" Marshall <trap15@raidenii.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////
// List of regions supported.
///////////////////////////////////////////////////////////////////////////////

REGION(0x0F, 7, "INVALID") /* Not known to exist, so should be safe? */
REGION(0x00, 4, "ZERO")
REGION(0x01, 5, "JAPAN")
REGION(0x10,13, "UNITED STATES")
REGION(0x20, 6, "TAIWAN")
REGION(0x22, 4, "ASIA")
REGION(0x24, 5, "KOREA")
REGION(0x28, 5, "CHINA")
REGION(0x80, 6, "EUROPE")
REGION(0x82, 7, "AUSTRIA")
REGION(0x8C,13, "GREAT BRITAIN")
REGION(0x90, 7, "HOLLAND")
REGION(0x92, 5, "ITALY")
REGION(0x9C,11, "SWITZERLAND")
REGION(0x9E, 9, "AUSTRALIA")
REGION(0xBE, 6, "WORLD?")
REGION(0xFF, 6, "ERASED")
REGION_ATTR(/*Count*/2+1+1+4+3+4+1+1, /*First index*/2, /*Hidden end values*/1)
