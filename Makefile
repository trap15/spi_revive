###############################################################################
## SPI REVIVE
###############################################################################
## Copyright (C)2018 Alex "trap15" Marshall <trap15@raidenii.net>
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to
## deal in the Software without restriction, including without limitation the
## rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
## sell copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
## FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
## IN THE SOFTWARE.
###############################################################################
## Makefile (kinda buggy, just use the scripts)
###############################################################################

GAME_SUPPORT:=rdftj rdft2j
GAME?=$(GAME_SUPPORT)

HOSTCC=cc
RM:=rm -rf
MKDIR:=mkdir -p

# Rock'n'roll
TGTCC=gcc
TGTLD=ld

TARGETS:=$(addsuffix .hex,$(addprefix build/,$(GAME)))

ifeq "" "$(GAME)"
$(error GAME must be specified)
endif

# Hell yeah I love Makefile
ifneq "$(GAME)" "$(foreach ONEGAME,$(GAME),$(filter $(ONEGAME),$(GAME_SUPPORT)))"
$(error Unsupported games in GAME list)
endif

# Precious phony suffixes!
.PRECIOUS:
.PHONY: all build clean
.SUFFIXES:

all: build
build: $(TARGETS)

build/%.o: src/main.S
	$(MKDIR) $(@D)
	$(TGTCC) -DGAMEID_$* -m32 -Isrc -c -o $@ $<
build/%.ld: src/ldscript.ld.in
	$(MKDIR) $(@D)
	cpp -P -DGAMEID_$* -Isrc -o $@ $<
build/%.exe: build/%.o build/%.ld
	$(MKDIR) $(@D)
	$(TGTLD) -m i386pe -nostdlib -Tbuild/$*.ld -o $@ $<
build/%.hex: build/%.exe
	$(MKDIR) $(@D)
	objcopy -S -j .prg1 -j .prg2 -Oihex $< $@

build/makerom: tool/makerom.c
	$(MKDIR) $(@D)
	$(HOSTCC) -o $@ $<

clean:
	$(RM) build/

# File dependencies
src/main.S: src/config.h src/hwreg.inc.S src/injected.inc.S src/selector.inc.S src/strings.inc.S
src/config.h: src/spi_region_table.inc.h
