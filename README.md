# SPI REVIVE

SPI REVIVE is a patch that can be applied to any (supported) Seibu SPI game,
allowing the cartridge to change the region flags on the motherboard.
This is particularly useful in the case of the region becoming corrupt, which
would normally render the motherboard useless.

A pre-built release can be downloaded from the Downloads section in the
bitbucket sidebar.

## What this patch does

This patch adds a region-change menu when the game boots up. It can be launched
by holding A and B while the game boots. The menu will also launch if the game
does not match the motherboard's region or the flash is corrupt.

This menu lets you select from a multitude of known regions, and displays the
cart's region and the current region of the motherboard. By pressing B from the
menu, one can leave the menu and attempt to boot the game.

You will need to set your jumpers for flashing. On SXX2C V2.0, this involves
moving JP071. Once the region change and game update is complete, feel free to
set the jumper back.

## Supported games

- Raiden Fighters (Japanese) (MAME set: rdftj)
- Raiden Fighters 2 (Japanese) (MAME set: rdft2j)

Adding games should be relatively trivial, all the game-specific data is in
config.h, and documented. Pull requests are accepted :)

## Using

To prepare the 4 ROMs for writing, dump your existing set. I'll refer to them
as `a.bin`, `b.bin`, `c.bin`, `d.bin`.

Now, you want to make a folder called `output`, then run the following:

	./makerom patch.hex a.bin b.bin c.bin d.bin

This will place `1.bin`, `2.bin`, `3.bin`, `4.bin` into `output`, which can be
burned to ROM (27C040 compatible) and used.

## Building

The makefile isn't very good at picking up file changes, so if you change the
source I recommend using a clean before each build.

To specify building only a single game, set the `GAME` environment variable
to the set name to build.

The output .hex files are placed in `build`.

## Legal

Copyright (C)2018 Alex "trap15" Marshall <trap15@raidenii.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
