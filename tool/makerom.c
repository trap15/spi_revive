///////////////////////////////////////////////////////////////////////////////
// SPI REVIVE
///////////////////////////////////////////////////////////////////////////////
// Copyright (C)2018 Alex "trap15" Marshall <trap15@raidenii.net>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to
// deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
// IN THE SOFTWARE.
///////////////////////////////////////////////////////////////////////////////
// Builds final ROM images from source binary and patch ihex.
///////////////////////////////////////////////////////////////////////////////

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define LOG(...) do { printf(__VA_ARGS__); fflush(stdout); } while(0)
#if DEBUG
# define DBG LOG
#else
# define DBG(...)
#endif

// These are only coincidentally the same!
#define ROM_OFFSET 0x200000
#define ROM_SIZE 0x200000

typedef enum {
  IhexState_StartCode,
  IhexState_UnitCount,
  IhexState_Address,
  IhexState_Type,
  IhexState_Data,
  IhexStateCount,
} IhexState;

typedef enum {
  IhexRecType_Data      = 0,
  IhexRecType_EOF       = 1,
  IhexRecType_ExtSeg    = 2,
  IhexRecType_Ignore3   = 3,
  IhexRecType_ExtLinear = 4,
  IhexRecType_Ignore5   = 5,
  IhexRecTypeCount,
} IhexRecType;

// Convert ASCII hex to numeric hex
int hexch(int ch) {
  if(ch >= '0' && ch <= '9') return ch - '0' + 0;
  if(ch >= 'a' && ch <= 'f') return ch - 'a' + 0xA;
  if(ch >= 'A' && ch <= 'F') return ch - 'A' + 0xA;
  return 0;
}
// Shift in a hex digit from ASCII
#define SHIFT_HEXCH(CH) do { val = (val << 4) | hexch(CH); } while(0)

// Number of hex digits in each unit for a record type
static const int s_record_unit_size[IhexRecTypeCount] = {
  [IhexRecType_Data]      = 2,
  [IhexRecType_EOF]       = 1,
  [IhexRecType_ExtSeg]    = 4,
  [IhexRecType_Ignore3]   = 8,
  [IhexRecType_ExtLinear] = 4,
  [IhexRecType_Ignore5]   = 8,
};

static const int s_state_value_size[IhexStateCount] = {
  [IhexState_StartCode] = 0,
  [IhexState_UnitCount] = 2,
  [IhexState_Address]   = 4,
  [IhexState_Type]      = 2,
  [IhexState_Data]      = 2,
};

// State transition core
#define STATE_TRANSITION_EX(NEWSTATE, LEFT) do { state = NEWSTATE; val = 0; left = LEFT; } while(0)
#define STATE_TRANSITION(NEWSTATE) STATE_TRANSITION_EX(NEWSTATE, s_state_value_size[NEWSTATE])

static int read_rom(uint8_t *rom, const char *fname, int stride) {
  uint8_t *buf = calloc(ROM_SIZE / stride, 1);
  assert(buf);

  LOG("Reading '%s'...\n", fname);
  FILE *ifp = fopen(fname, "rb");
  if(ifp == NULL) {
    LOG("Can't open '%s' for reading.\n", fname);
    return 0;
  }

  fread(buf, ROM_SIZE / stride,1, ifp);
  fclose(ifp);

  for(int i = 0; i < ROM_SIZE; i += stride) {
    rom[i] = buf[i / stride];
  }
  return 1;
}

int main(int argc, const char *argv[]) {
  if((argc != 3) && (argc != 6)) {
    LOG("Invalid arguments. Usage:\n\t%s patch.hex orig1_or_combined.bin [orig2.bin orig3.bin orig4.bin]\n");
    return 0;
  }

  uint8_t *buf = calloc(ROM_SIZE, 1);
  if(argc == 3) {
    assert(read_rom(buf+0, argv[2], 1));
  }else if(argc == 6) {
    assert(read_rom(buf+0, argv[2], 4));
    assert(read_rom(buf+1, argv[3], 4));
    assert(read_rom(buf+2, argv[4], 4));
    assert(read_rom(buf+3, argv[5], 4));
  }

  FILE *ifp = fopen(argv[1],"rb");

  LOG("Applying patch '%s'...\n", argv[1]);

  // All the state for the ihex interpreter
  IhexState state = IhexState_StartCode;
  uint8_t unit_count;
  uint16_t address;
  uint32_t addr_base;
  uint32_t val;
  int rec_type = IhexRecType_Data;
  int left = 0;
  do {
    int ch = fgetc(ifp);
    if(ch == EOF) // End on EOF, even though it should stop on EOF record instead.
      break;

    // Start code waits for `:`, then begins normal state machine
    if(state == IhexState_StartCode) {
      if(ch == ':') {
        STATE_TRANSITION(IhexState_UnitCount);
      }
    }else{
      SHIFT_HEXCH(ch);
      if(--left == 0) {
        switch(state) {
          case IhexState_UnitCount:
            unit_count = val;
            STATE_TRANSITION(IhexState_Address);
            break;
          case IhexState_Address:
            address = val;
            STATE_TRANSITION(IhexState_Type);
            break;
          case IhexState_Type:
            rec_type = val;
            assert(rec_type <= IhexRecTypeCount);

            // Amount of digits in each unit is different per record.
            STATE_TRANSITION_EX(IhexState_Data, s_record_unit_size[rec_type]);

            // Always 1 unit for non-data records
            if(rec_type != IhexRecType_Data) {
              unit_count = 1;
            }
            break;
          case IhexState_Data:
            switch(rec_type) {
              case IhexRecType_Data: { // Data
                uint32_t real_addr = addr_base + address - ROM_OFFSET;
                DBG("Overwrite buf[%06X](%02X) with %02X\n", real_addr, buf[real_addr], val);
                buf[real_addr] = val;
                address++;
                break;
              }
              case IhexRecType_EOF: // End of File
                break;
              case IhexRecType_ExtSeg: // Extended segment address
                addr_base = val << 4;
                break;
              case IhexRecType_Ignore3: // Ignored
                break;
              case IhexRecType_ExtLinear: // Extended linear address
                addr_base = val << 16;
                break;
              case IhexRecType_Ignore5: // Ignored
                break;
              default:
                assert(0);
                break;
            }
            // When out of units, return to start code
            if(--unit_count == 0) {
              STATE_TRANSITION(IhexState_StartCode);
            }else{ // Otherwise loop
              STATE_TRANSITION_EX(IhexState_Data, s_record_unit_size[rec_type]);
            }
            break;
          default:
            assert(0);
            break;
        }
      }
    }
    // Record type EOF is one way to end processing
  } while(rec_type != IhexRecType_EOF);
  LOG("Patching completed\n");

  FILE *ofps[4];
  LOG("Writing ROM images...\n");
  ofps[0] = fopen("output/1.bin","wb");
  ofps[1] = fopen("output/2.bin","wb");
  ofps[2] = fopen("output/3.bin","wb");
  ofps[3] = fopen("output/4.bin","wb");
  for(int i = 0; i < ROM_SIZE/4; i++) {
    uint8_t *bufptr = buf+(i << 2);
    fwrite(bufptr+0, 1, 1, ofps[0]);
    fwrite(bufptr+1, 1, 1, ofps[1]);
    fwrite(bufptr+2, 1, 1, ofps[2]);
    fwrite(bufptr+3, 1, 1, ofps[3]);
  }
  LOG("Writing complete\n");
  return 0;
}
