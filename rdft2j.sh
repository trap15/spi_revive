#!/bin/sh
###############################################################################
## SPI REVIVE
###############################################################################
## Copyright (C)2018 Alex "trap15" Marshall <trap15@raidenii.net>
##
## Permission is hereby granted, free of charge, to any person obtaining a copy
## of this software and associated documentation files (the "Software"), to
## deal in the Software without restriction, including without limitation the
## rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
## sell copies of the Software, and to permit persons to whom the Software is
## furnished to do so, subject to the following conditions:
##
## The above copyright notice and this permission notice shall be included in
## all copies or substantial portions of the Software.
##
## THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
## IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
## FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
## AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
## LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
## FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
## IN THE SOFTWARE.
###############################################################################
## Builds and applies patch to rdft2j
###############################################################################

make clean
make GAME=rdft2j
make build/makerom
mkdir -p output/
./build/makerom build/rdft2j.hex rom/rdft2j.bin
cp output/1.bin /d/roms/mame/rdft2u/1.bin
cp output/2.bin /d/roms/mame/rdft2u/2.bin
cp output/3.bin /d/roms/mame/rdft2u/3.bin
cp output/4.bin /d/roms/mame/rdft2u/4.bin
